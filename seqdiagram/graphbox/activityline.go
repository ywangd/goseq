package graphbox

import (
    "fmt"
)


type ActivityArrowStem int
const (
    SolidArrowStem  ActivityArrowStem   =   iota
    DashedArrowStem                     =   iota
    ThickArrowStem                      =   iota
)



type ActivityLineStyle struct {
    Font            Font
    FontSize        int
    Margin          Point
    TextGap         int
    SelfRefWidth    int
    SelfRefHeight   int
    //ArrowHead       ActivityArrowHead
    ArrowHead       *ArrowHeadStyle
    ArrowStem       ActivityArrowStem
}

// Returns the text style
func (as ActivityLineStyle) textStyle() string {
    s := SvgStyle{}

    s.Set("font-family", as.Font.SvgName())
    s.Set("font-size", fmt.Sprintf("%dpx", as.FontSize))

    return s.ToStyle()
}


// An activity arrow
type ActivityLine struct {
    TC              int
    style           ActivityLineStyle
    textBox         *TextBox
    textBoxRect     Rect
}

func NewActivityLine(toCol int, selfRef bool, text string, style ActivityLineStyle) *ActivityLine {
    var textBoxAlign TextAlign = MiddleTextAlign
    if selfRef {
        textBoxAlign = LeftTextAlign
    }

    textBox := NewTextBox(style.Font, style.FontSize, textBoxAlign)
    textBox.AddText(text)

    brect := textBox.BoundingRect()
    return &ActivityLine{toCol, style, textBox, brect}
}

func (al *ActivityLine) Constraint(r, c int, applier ConstraintApplier) {
    h := al.textBoxRect.H + al.style.Margin.Y + al.style.TextGap
    w := al.textBoxRect.W

    lc, rc := c, al.TC
    if al.TC < c {
        lc, rc = al.TC, c
    }

    if al.TC == c {
        // An arrow referring to itself
        w = maxInt(w, al.style.SelfRefWidth)
        h += al.style.TextGap / 2

        applier.Apply(AddSizeConstraint{r, c, 0, 0, h, al.style.Margin.Y + al.style.SelfRefHeight})
        applier.Apply(TotalSizeConstraint{r - 1, lc, r, lc + 1, w + al.style.Margin.X * 2, 0})
    } else {
        applier.Apply(AddSizeConstraint{r, c, 0, 0, h, al.style.Margin.Y})
        applier.Apply(TotalSizeConstraint{r - 1, lc, r, rc, w + al.style.Margin.X * 2, 0})
    }
}

func (al *ActivityLine) Draw(ctx DrawContext, point Point) {
    fx, fy := point.X, point.Y

    if ctx.C == al.TC {
        // A self reference arrow
        if point, isPoint := ctx.PointAt(ctx.R, ctx.C + 1) ; isPoint {
            // Draw an arrow referencing itself
            ty := point.Y
            stemX, stemY := fx + al.style.SelfRefWidth, ty + al.style.SelfRefHeight

            textX := fx + al.style.TextGap * 2
            textY := ty - al.style.TextGap - al.style.TextGap / 2
            al.renderMessage(ctx, textX, textY, true)

            al.drawArrowStem(ctx, fx, fy, stemX, ty)
            al.drawArrowStem(ctx, stemX, fy, stemX, stemY)
            al.drawArrowStem(ctx, stemX, stemY, fx, stemY)
            al.drawArrow(ctx, fx, stemY, false)
        }
    } else {

        if point, isPoint := ctx.PointAt(ctx.R, al.TC) ; isPoint {
            tx, ty := point.X, point.Y

            textX := fx + (tx - fx) / 2
            textY := ty - al.style.TextGap
            al.renderMessage(ctx, textX, textY, false)
            al.drawArrowStem(ctx, fx, fy, tx, ty)
            al.drawArrow(ctx, tx, ty, al.TC > ctx.C)
        }
    }
}

// Draws the arrow stem
func (al *ActivityLine) drawArrowStem(ctx DrawContext, fx, fy, tx, ty int) {
    switch al.style.ArrowStem {
    case SolidArrowStem:
        ctx.Canvas.Line(fx, fy, tx, ty, "stroke:black;stroke-width:2px;")
    case DashedArrowStem:
        ctx.Canvas.Line(fx, fy, tx, ty, "stroke:black;stroke-dasharray:4,2;stroke-width:2px;")
    case ThickArrowStem:
        ctx.Canvas.Line(fx, fy, tx, ty, "stroke:black;stroke-width:4px;")
    }
}

func (al *ActivityLine) renderMessage(ctx DrawContext, tx, ty int, anchorLeft bool) {
    //rect, textPoint := MeasureFontRect(al.style.Font, al.style.FontSize, al.Text, tx, ty, SouthGravity)
    anchor := SouthGravity
    if anchorLeft {
        anchor = SouthWestGravity
    }

    rect := al.textBoxRect.PositionAt(tx, ty, anchor)

    ctx.Canvas.Rect(rect.X, rect.Y, rect.W, rect.H, "fill:white;stroke:white;")
    al.textBox.Render(ctx.Canvas, tx, ty, anchor)
}

// Draws the arrow head.
func (al *ActivityLine) drawArrow(ctx DrawContext, x, y int, isRight bool) {
    headStyle := al.style.ArrowHead

    var xs, ys = make([]int, len(headStyle.Xs)), make([]int, len(headStyle.Ys))
    if len(xs) != len(ys) {
        panic("length of xs and ys must be the same")
    }

    for i := range headStyle.Xs {
        ox, oy := headStyle.Xs[i], headStyle.Ys[i]
        if isRight {
            xs[i] = x + ox
        } else {
            xs[i] = x - ox
        }
        ys[i] = y + oy
    }

    ctx.Canvas.Polyline(xs, ys, StyleFromString(headStyle.BaseStyle).ToStyle())
}

// Style information for arrow heads
type ArrowHeadStyle struct {
    // Points from the origin
    Xs              []int
    Ys              []int

    // Base style for the arrow head
    //BaseStyle       SvgStyle
    BaseStyle       string
}

//type ArrowHeadStyles map[ActivityArrowHead]*ArrowHeadStyle 

// Styling of the arrow head
/*
var ArrowHeadStyles = map[ActivityArrowHead]*ArrowHeadStyle {
    SolidArrowHead: &arrowHeadStyle {
        Xs: []int { -9, 0, -9 },
        Ys: []int { -5, 0, 5 },
        BaseStyle: StyleFromString("stroke:black;fill:black;stroke-width:2px;"),
    },
    OpenArrowHead: &arrowHeadStyle {
        Xs: []int { -9, 0, -9 },
        Ys: []int { -5, 0, 5 },
        BaseStyle: StyleFromString("stroke:black;fill:none;stroke-width:2px;"),
    },
    BarbArrowHead: &arrowHeadStyle {
        Xs: []int { -11, 0 },
        Ys: []int { -7, 0 },
        BaseStyle: StyleFromString("stroke:black;fill:black;stroke-width:2px;"),
    },
    LowerBarbArrowHead: &arrowHeadStyle {
        Xs: []int { -11, 0 },
        Ys: []int { 7, 0 },
        BaseStyle: StyleFromString("stroke:black;fill:black;stroke-width:2px;"),
    },
}
*/