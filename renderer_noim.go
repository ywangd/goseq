// Renderers disabled if noim is specified
//

//+build noim

package main

import (
    "errors"
    "bitbucket.org/ywangd/goseq/seqdiagram"
)

func PngRenderer(diagram *seqdiagram.Diagram, style *seqdiagram.DiagramStyles, target string) error {
    return errors.New("PNG renderer not available")
}
